const glob = require('glob');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const path = require('path');
const paths = require('./paths');
const publicPath = '/';


const getFileName = function(filePath){
  let filePathArray = filePath.split('/');
  let pathFolder =  filePathArray[filePathArray.length-2];
  return pathFolder === 'src' ? 'home': pathFolder;
};

//多入口配置
// 通过glob模块读取pages文件夹下的所有对应文件夹下的js后缀文件，如果该文件存在
exports.entries = function() {
  const map = {};
  const entryFiles = glob.sync(paths.appSrc + '/**/index.js');
  entryFiles.forEach((filePath) => {
    const conf = [];
    let filename = getFileName(filePath);
    if (process.env.NODE_ENV !== 'production') {
      conf.push(require.resolve('react-dev-utils/webpackHotDevClient'));
    }
    conf.push(filePath);
    map[filename] = conf.slice(0);
    conf.length = 0;
  });
  return map
};

exports.output = {
  // Add /* filename */ comments to generated require()s in the output.
  pathinfo: true,
  // This does not produce a real file. It's just the virtual path that is
  // served by WebpackDevServer in development. This is the JS bundle
  // containing code from all our entry points, and the Webpack runtime.
  //修改此处，防止JS名称重复
  filename: 'static/js/[name].[hash:8].js',
  chunkFilename: 'static/js/[name].[hash:8].chunk.js',
  // This is the URL that app is served from. We use "/" in development.
  publicPath: publicPath,
  // Point sourcemap entries to original disk location (format as URL on Windows)
  devtoolModuleFilenameTemplate: info =>
  path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'),
};

//多页面输出配置
// 与上面的多页面入口配置相同，读取pages文件夹下的对应的html后缀文件，然后放入数组中
exports.htmlPlugin = function() {
  const entryHtml = glob.sync(paths.appSrc + '/**/index.js');
  let arr = [];
  entryHtml.forEach((filePath) => {
    let filename = getFileName(filePath);
    const fileResolvePath = filePath.split('/src/')[1].replace('pages/', '').replace('.js', '.html');
    let conf = {
      inject: true,
      chunks: [filename],
      template: paths.appHtml,
      filename: fileResolvePath
    };
    if (process.env.NODE_ENV === 'production') {
      conf = merge(conf, {
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true,
        }
      })
    }
    arr.push(new HtmlWebpackPlugin(conf))
  });
  return arr
};
