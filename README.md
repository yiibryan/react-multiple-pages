# React多页面应用打包

## 主要修改地方

1. package.json 添加字段 `"homepage": "/mul-react/",`, 添加包 `webpack-merge`
2. 添加multiple-util.js文件
3. 修改webpack.config.dev.js
    ```
    line 76  
        const utils = require('./multiple-util.js');  
    line 106   
        entry: utils.entries()  
    line 107  
        output: utils.output  
    line 412  
        ].concat(utils.htmlPlugin()).filter(Boolean),  
    ```
4. webpack.config.prod.js
    ```
    line 119 
        entry: utils.entries(),
    line 429
        ].concat(utils.htmlPlugin()).filter(Boolean),  
    ```
